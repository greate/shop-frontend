import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CartService } from '@services/cart.service';
import { IColor, IMaterial, IProduct, ISize, IType } from '@models/index';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  public form: FormGroup;
  public showMsg: boolean = false;

  @Input() public product: IProduct;
  @Input() public colors: IColor[];
  @Input() public types: IType[];
  @Input() public sizes: ISize[];
  @Input() public materials: IMaterial[];

  constructor(private cartService: CartService) { }

  ngOnInit() {
    this.form = new FormGroup({
      material: new FormControl(this.materials[0]?.id, Validators.required),
      color: new FormControl(this.colors[0]?.id, Validators.required),
      type: new FormControl(this.types[0]?.id, Validators.required),
      size: new FormControl(this.colors[0]?.id, Validators.required),
    });
  }

  addItem(product: IProduct) {
    const { material, color, type, size } = this.form.value;
    this.cartService.addItem(product, material, color, type, size);
    this.showMsg = true;
    setTimeout(() => {
      this.showMsg = false;
    }, 1500);
  }
}
