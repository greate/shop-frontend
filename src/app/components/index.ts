import { CARDS } from "./cards";
import { LAYOUTS } from "./layouts";
import { PAGES } from "./pages";

export const COMPONENTS = [
    ...PAGES,
    ...LAYOUTS,
    ...CARDS
];

