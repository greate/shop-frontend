import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '@services/users.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  public isAuthed = false;
  constructor(private usersApi: UsersService, private router: Router) { }

  ngOnInit(): void {
    this.usersApi.auth$.subscribe((data) => this.isAuthed = data);
    this.isAuthed = !!localStorage.getItem('token');
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('email');
    this.router.navigateByUrl('/login');
    this.usersApi.auth$.next(false);
  }
}
