import { Component, OnInit } from '@angular/core';
import { IColor, IMaterial, IProduct, ISize, IType } from '@models/index';
import { ProductsApiService } from '@services/products-api.service';
import { CartService } from '@services/cart.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {

  public products: IProduct[] = [];
  public colors: IColor[] = [];
  public types: IType[] = [];
  public sizes: ISize[] = [];
  public materials: IMaterial[] = [];

  loading = false;

  constructor(private productsApi: ProductsApiService, private cartService: CartService) { }

  ngOnInit(): void {
    this.loading = true;

    this.productsApi.loadProducts().subscribe((data) => {
      this.loading = false;
      this.products = data;
    });

    this.productsApi.getColors().subscribe((data) => this.colors = data);
    this.productsApi.getTypes().subscribe((data) => this.types = data);
    this.productsApi.getSizes().subscribe((data) => this.sizes = data);
    this.productsApi.getMaterials().subscribe((data) => this.materials = data);
  }

  isLoaded() {
    return this.products.length && this.colors.length && this.types.length && this.sizes.length && this.materials.length;
  }

  getCartItem(product: IProduct[]) {
    return this.cartService.getItem(product);
  }
}
