import { Component, OnInit } from '@angular/core';
import { ProductsApiService } from '@services/products-api.service';
import { IColor, IMaterial, IProduct, ISize, IType } from '@models/index';
import { CartService } from '@services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {

  public colors: IColor[];
  public types: IType[];
  public sizes: ISize[];
  public materials: IMaterial[];

  cart: any = [];
  newCart: any = [];
  id: number;
  loading = true;

  constructor(private cartService: CartService, private productsApi: ProductsApiService) { }

  async ngOnInit() {
    this.loading = false;
    this.productsApi.getColors().subscribe((data) => this.colors = data);
    this.productsApi.getTypes().subscribe((data) => this.types = data);
    this.productsApi.getSizes().subscribe((data) => this.sizes = data);
    this.productsApi.getMaterials().subscribe((data) => this.materials = data);
    this.cartService.getCartItems();
  }

  isLoaded() {
    return this.colors && this.types && this.sizes && this.materials;
  }

  addProductCount(product: IProduct) {
    this.cartService.addProductCount(product);
  }

  removeProductCount(product: IProduct) {
    this.cartService.removeProductCount(product);
  }

  removeItem(product: IProduct) {
    this.cartService.removeItem(product);
  }

  get totalPrice() {
    return this.cartService.totalPrice();
  }

  get products() {
    this.cart = this.cartService.getAllItems();
    return this.cart;
  }

  getItem(items: any[], id: number) {
    return items.filter((data) => id === data.id)[0];
  }
}
