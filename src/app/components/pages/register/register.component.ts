import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from '@services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public form = new FormGroup({
    name: new FormControl(null, Validators.required),
    address: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)),
    email: new FormControl(null, Validators.email),
    password: new FormControl(null, Validators.required),
  });

  public errorMessage: string = '';

  constructor(private usersApi: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  signUp() {
    const {name, address, phone, email, password} = this.form.value;
    this.usersApi.createUser({
      name,
      address,
      phone,
      email,
      password
    }).subscribe((res) => {
      this.errorMessage = Object.values(res)[0];
      if (this.errorMessage === 'Successful signUp') {
        this.router.navigateByUrl('/login');
        alert('Successful SignUp!');
      }
    });
  }
}
