import { CartComponent } from "./cart/cart.component";
import { CheckoutComponent } from "./checkout/checkout.component";
import { LoginComponent } from "./login/login.component";
import { NotFoundPageComponent } from "./not-found-page/not-found-page.component";
import { ProductsComponent } from "./products/products.component";
import { RegisterComponent } from "./register/register.component";

export const PAGES = [
    NotFoundPageComponent,
    CartComponent,
    ProductsComponent,
    LoginComponent,
    RegisterComponent,
    CheckoutComponent
];

