import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CartService } from '@services/cart.service';
import { CheckoutService } from '@services/checkout.service';
import { UsersService } from '@services/users.service';
import { IDelivery, IPayment } from '@models/index';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  public form: FormGroup;
  public paymentMethods: IPayment[] = [];
  public endpoint = environment.endpoint;
  public loading = false;
  public id: number;
  public orderId: number;

  @Input() public payments: IPayment[];
  @Input() public deliveries: IDelivery[];

  constructor(private cartService: CartService, private checkoutApi: CheckoutService, private usersApi: UsersService, private router: Router) { }

  async ngOnInit() {
    this.loading = true;
    this.id = (await this.usersApi.getUserId().toPromise()).msg;
    this.checkoutApi.getPayments().subscribe(
      (data: IPayment[]) => {
        this.loading = false;
        this.paymentMethods = data;
        this.form = new FormGroup({
          payment: new FormControl(this.paymentMethods[0]?.id, Validators.required),
          delivery: new FormControl('1', Validators.required),
        });
      }
    );
  }

  isLoaded() {
    return this.paymentMethods.length;
  }

  async createOrder() {
    const { payment, delivery } = this.form.value;

    this.orderId = (await this.checkoutApi.createOrder({
      id_client: this.id,
      id_delivery: delivery,
      id_payment: payment,
      total_price: this.cartService.totalPrice()
    }).toPromise()).msg;

    if (this.orderId) {
      this.router.navigateByUrl('/');
      this.cartService.getCartItems();
      alert('Order successfully created!')
    }
  }

  getOrderId() {
    return this.orderId;
  }
}
