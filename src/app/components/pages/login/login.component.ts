import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CartService } from '@services/cart.service';
import { UsersService } from '@services/users.service';
import { IResponse } from '@models/index';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public errorMessage: string = '';

  public emailControl = new FormControl(null, [Validators.email, Validators.required]);
  public passwordControl = new FormControl(null, [Validators.maxLength(32), Validators.minLength(4), Validators.required]);

  constructor(private usersApi: UsersService, private router: Router, private cartApi: CartService) { }

  ngOnInit(): void { }

  login(email: string, password: string) {
    localStorage.setItem('email', email);
    this.usersApi.login({
      email,
      password
    }).subscribe((res: IResponse) => {
      if (res.success) {
        localStorage.setItem('token', res.msg);
        this.router.navigateByUrl('/');
        this.usersApi.auth$.next(true);
        this.cartApi.getCartItems();
        return;
      }
      this.errorMessage = res.msg;
    });
  }
}
