import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public isAuthed = false;
  public authUrlList = ['/login', '/register'];

  checkUrl(url: string): boolean {
    return this.authUrlList.some((item) => item === url);
  }

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    this.isAuthed = !!localStorage.getItem('token');

    if (!this.isAuthed && !this.checkUrl(state.url)) {
      this.router.navigate(['login']);
      return false;
    }
    if (this.isAuthed && this.checkUrl(state.url)) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }
}
