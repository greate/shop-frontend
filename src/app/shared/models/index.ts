export interface IDelivery {
    type: string;
    price: number;
    status: string;
}

export interface IResponse<res = string> {
    success: boolean;
    msg: res;
}

export interface ILogin {
    email: string;
    password: string;
}

export interface IOption {
    id_option: number;
    id_material: number;
    id_product: number;
    id_type: number;
    id_color: number;
    id_size: number;
}

export interface IOrder {
    id_client: number;
    total_price: number;
    id_payment: number;
    id_delivery: number;
    status: string
}

export interface IPayment {
    id: number;
    name: string;
}

export interface IProduct {
    id: number
    id_product: number,
    name: string
    id_material: number,
    material: string
    id_color: number,
    color: string
    id_type: number,
    type: string
    id_size: number,
    size: string
    price: number
    available: number
}

export interface IRegister {
    name: string;
    address: string;
    phone: string;
    email: string;
    password: string;
}

export interface IUser {
    name: string;
    address: string;
    phone: string;
    email: string;
    password: string;
}

export interface IColor {
    id: number;
    color: string;
}

export interface IType {
    id: number;
    type: string;
}

export interface ISize {
    id: number;
    size: string;
}

export interface IMaterial {
    id: number;
    material: string;
}


