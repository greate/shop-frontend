import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IColor, IOption, IProduct, IType, ISize, IMaterial } from '@models/index';


@Injectable({
  providedIn: 'root'
})
export class ProductsApiService {

  public endpoint = environment.endpoint;
  products: IProduct[] = [];
  product: any = {};

  constructor(private http: HttpClient) { }

  loadProducts() {
    return this.http.get<IProduct[]>(`${this.endpoint}/products/`);
  }

  getOptionId(body: Partial<IOption>): Observable<any> {
    return this.http.post(`${this.endpoint}/options/getOption`, body);
  }

  getColors() {
    return this.http.get<IColor[]>(`${this.endpoint}/color/`);
  }

  getTypes() {
    return this.http.get<IType[]>(`${this.endpoint}/type/`);
  }

  getSizes() {
    return this.http.get<ISize[]>(`${this.endpoint}/size/`);
  }

  getMaterials() {
    return this.http.get<IMaterial[]>(`${this.endpoint}/material/`);
  }
}
