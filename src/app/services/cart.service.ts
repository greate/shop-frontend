import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResponse } from '@models/index';
import { ProductsApiService } from './products-api.service';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public endpoint = environment.endpoint;
  cart: any = [];

  async getCartItems() {
    const id = (await this.usersApi.getUserId().toPromise()).msg;
    this.getCartFromDb(id).subscribe((data) => {
      this.cart = data;
    })
  }

  constructor(private http: HttpClient, private usersApi: UsersService, private productsApi: ProductsApiService) { }

  getCartFromDb(client_id: any) {
    return this.http.get(`${this.endpoint}/cart/${client_id}`);
  }

  pushOptionToDb(body: any) {
    return this.http.post(`${this.endpoint}/options`, body);
  }

  pushProductToCart(body: any) {
    return this.http.post(`${this.endpoint}/cart`, body);
  }

  deleteProductFromCart(body: any) {
    return this.http.post(`${this.endpoint}/cart/deleteCart`, body);
  }

  pushProductToOrderDetails(body: any) {
    return this.http.post(`${this.endpoint}/order-details`, body);
  }

  getOptionId(id_material: number, id_product: number, id_type: number, id_color: number, id_size: number): Observable<IResponse<number>> {
    return this.productsApi.getOptionId({
      id_material,
      id_product,
      id_type,
      id_color,
      id_size
    });
  }

  async addItem(product: any, material: number, color: number, type: number, size: number) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name
        && this.cart[item].color === color
        && this.cart[item].material === material
        && this.cart[item].size === size
        && this.cart[item].type === type) {
        if (this.cart[item].count < this.cart[item].available) {
          this.cart[item].count++;
          this.pushProductToCart({
            id_client: (await this.usersApi.getUserId().toPromise()).msg,
            id_tshirt: product.id,
            id_option: (await this.getOptionId(+material, product.id, +type, +color, +size).toPromise()).msg,
            count: this.cart[item].count
          }).subscribe();
        }
        return;
      }
    }

    this.cart.push({
      product_id: product.id,
      name: product.name,
      price: product.price,
      available: product.available,
      material,
      color,
      type,
      size,
      count: 1
    });
    this.pushOptionToDb({
      id_material: material,
      id_product: product.id,
      id_type: type,
      id_color: color,
      id_size: size
    }).subscribe();
    this.pushProductToCart({
      id_client: (await this.usersApi.getUserId().toPromise()).msg,
      id_tshirt: product.id,
      id_option: (await this.getOptionId(+material, product.id, +type, +color, +size).toPromise()).msg,
      count: 1
    }).subscribe();
  }

  async addProductCount(product: any) {
    const id = this.cart.findIndex((item) => JSON.stringify(product) === JSON.stringify(item));

    if (id != -1) {
      if (this.cart[id].count < this.cart[id].available) {
        this.cart[id].count++;
      }
      this.pushProductToCart({
        id_client: (await this.usersApi.getUserId().toPromise()).msg,
        id_tshirt: product.product_id,
        id_option: (await this.getOptionId(product.material, product.product_id, product.type, product.color, product.size).toPromise()).msg,
        count: this.cart[id].count
      }).subscribe();
    }
  }

  async removeProductCount(product: any) {
    const id = this.cart.findIndex((item) => JSON.stringify(product) === JSON.stringify(item));

    if (id != -1) {
      this.cart[id].count--;
      if (this.cart[id].count === 0) {
        this.cart.splice(id, 1);
        this.deleteProductFromCart({
          id_client: (await this.usersApi.getUserId().toPromise()).msg,
          id_tshirt: product.product_id,
          id_option: (await this.getOptionId(product.material, product.product_id, product.type, product.color, product.size).toPromise()).msg
        }).subscribe();
      } else {
        this.pushProductToCart({
          id_client: (await this.usersApi.getUserId().toPromise()).msg,
          id_tshirt: product.product_id,
          id_option: (await this.getOptionId(product.material, product.product_id, product.type, product.color, product.size).toPromise()).msg,
          count: this.cart[id].count
        }).subscribe();
      }
    }
  }

  async removeItem(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        this.cart.splice(item, 1);
        this.deleteProductFromCart({
          id_client: (await this.usersApi.getUserId().toPromise()).msg,
          id_tshirt: product.product_id,
          id_option: (await this.getOptionId(product.material, product.product_id, product.type, product.color, product.size).toPromise()).msg
        }).subscribe();
        break;
      }
    }
  }

  totalPrice() {
    return this.cart.reduce((acc, item) => acc + item.price * item.count, 0);
  }

  getItem(product: any) {
    for (var item in this.cart) {
      if (this.cart[item].name === product.name) {
        return this.cart[item].count;
      }
    }
    return;
  }

  getAllItems() {
    return this.cart;
  }
}
