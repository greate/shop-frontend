import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IResponse, IUser, IRegister, ILogin } from '@models/index';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public endpoint = `${environment.endpoint}/user`;
  users: IUser[] = [];
  public auth$ = new Subject<boolean>();
  public email: string;
  public id_client: number;

  getEmail() {
    this.email = localStorage.getItem('email');
  }

  constructor(private http: HttpClient) { }

  getUserId(): Observable<IResponse<number>> {
    this.getEmail();
    return this.getUserIdByEmail(this.email);
  }

  getUsers() {
    return this.http.get<IUser[]>(`${this.endpoint}/options/`);
  }

  login(body: ILogin) {
    return this.http.post(`${this.endpoint}/login`, body);
  }

  createUser(body: IRegister) {
    return this.http.post(`${this.endpoint}/create`, body);
  }

  getUserIdByEmail(email: string): Observable<any> {
    return this.http.get(`${this.endpoint}/getUser/${email}`);
  }
}
