import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IOrder, IDelivery } from '@models/index';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  public endpoint = environment.endpoint;
  public email: string;
  public id_client: number;

  constructor(private http: HttpClient) { }

  createOrder(body: Partial<IOrder>): Observable<any>  {
    return this.http.post(`${this.endpoint}/order/create`, body);
  }

  getOrder(body: Partial<IOrder>) {
    return this.http.post(`${this.endpoint}/order/getOrder`, body);
  }

  getPaymentMethods() {
    return this.http.get(`${this.endpoint}/payments/`);
  }

  getDeliveryMethods() {
    return this.http.get(`${this.endpoint}/shipping/`);
  }

  addDelivery(body: IDelivery) {
    return this.http.post(`${this.endpoint}/shipping/`, body);
  }

  getPayments() {
    return this.http.get(`${this.endpoint}/payment/`);
  }
}
